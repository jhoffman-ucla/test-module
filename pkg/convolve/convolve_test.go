package convolve

import (
	"testing"
	"reflect"
	"math/rand"
)

func TestConvolve(t *testing.T) {
	cases := []struct {
		kernel	[]float64
		value	[]float64
		expected	[]float64
	}{
		{// Case: convolution of two integers
			kernel: []float64{3},
			value: []float64{3},
			expected: []float64{9},
		},
		{// Case: convolution of kernel and slice with odd, same lengths
			kernel: []float64{0,0,0},
			value: []float64{1,2,3},
			expected: []float64{0,0,0},
		},
		{// Case: convolution of kernel and slice with even, same lengths
			kernel: []float64{1,2,3,4},
			value: []float64{1,2,3,4},
			expected: []float64{4,10,20,25},
		},
	}

	for _, c := range cases {
		testconv := Convolve(c.value, c.kernel)
		if !reflect.DeepEqual(testconv, c.expected) {
			t.Errorf("Failed because expected %v, got %v", c.expected, testconv)
		}
	}
}

func BenchmarkConvolve(b *testing.B) {
	bencharray_int := rand.Perm(367)
	benchkernel_int := rand.Perm(367)
	
	bencharray := make([]float64, len(bencharray_int))
	for i, v := range bencharray_int {
		bencharray[i] = float64(v)
	}	
	
	benchkernel := make([]float64, len(benchkernel_int))
	for i, v := range benchkernel_int {
		benchkernel[i] = float64(v)
	}

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		Convolve(bencharray, benchkernel)
	}
}