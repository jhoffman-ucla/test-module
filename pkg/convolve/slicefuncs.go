package convolve

import "math"

func GenericMax[T int | float64](a, b T) T {
	// Returns maximum of two input numbers with the same type as input
	return T(math.Max(float64(a),float64(b)))
}

func GenericAbs[T int | float64](num T) T {
	// Returns absolute value of an input number with the same type as input
	return T(math.Abs(float64(num)))
}