package convolve

func Convolve(sino, kernel []float64, opts ...Option) []float64 {
	// Returns the convolution of size given by the "mode" option.
	// "valid" returns a convolution slice equal to the input sinogram slice.
	// "max" returns a convolution slice equal to max(len(sino),len(kernel)).

	// Configure convolve function with desired options (only mode so far)
	o := configOptions(opts)
	mode := o.Mode

	return convolution1d(sino, kernel, mode)
}

func convolution1d(sino, kernel []float64, mode string) []float64 {
	// Set parameters of slices depending on boundary mode for slice sizes and index offsets
	nsino := len(sino)
	nkernel := len(kernel)
	nconv := nsino // Sets default value of nconv for "valid" mode
	if mode == "max" { // Reset value of nconv if "max" mode is specified
		nconv = GenericMax(nsino, nkernel)
	}

	// Set offsets for discrete convolution indices
	offset_even := (nconv - GenericAbs(nsino-nkernel)) / 2 - 1  // Sets default offset assuming nsino >= nkernel
	offset_odd := offset_even + 1
	if nsino < nkernel && mode == "valid" { 	  // For "valid", resets offset if nsino < nkernel for proper
		offset_even += GenericAbs(nsino-nkernel)  // windowing of the convolution slice.
		offset_odd += GenericAbs(nsino-nkernel)
	}

	// Perform the convolution in for loops
	conv := make([]float64, nconv)
	for i := 0; i < nconv; i++ {
		for j := 0; j < nkernel; j++ {
			if i-j+offset_even >= 0 && i-j+offset_even < nsino && nconv%2 == 0 {
				conv[i] += sino[i-j+offset_even] * kernel[j]
			} else if i-j+offset_odd >= 0 && i-j+offset_odd < nsino && nconv%2 == 1 {
				conv[i] += sino[i-j+offset_odd] * kernel[j]
			}
		}
	}
	return conv
}