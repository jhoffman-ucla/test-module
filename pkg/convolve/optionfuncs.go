package convolve

import logrus "github.com/sirupsen/logrus"

// Define possible options for the convolution
type options struct {
	Mode string // Valid options are "valid" and "max"
}

// Define functional options -- to use Convolve, user must specify options in function form
type Option func(o *options)

func Mode(mode string) Option {
	// Overrides the default mode "valid" with a user-defined mode.
	return func(o *options) {
		o.Mode = mode
	}
}

func configOptions(opts []Option) options {
	// Returns an options struct with the user-specified options.
	o := options{Mode: "valid",} // Defines a default options struct
	for _, opt := range opts { // Overrides default with user input
		opt(&o)
	}
	
	if opts != nil { // Exits the program if the user enters an invalid option for mode
		if o.Mode != "valid" && o.Mode != "max" {
			logrus.Fatalf("Boundary selection mode ''%s'' does not exist. \n Change your selection to ''valid'' or ''max''",o.Mode)
		}
	}
	return o
}