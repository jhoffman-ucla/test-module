package main

import (
	"fmt"
	"gitlab.com/jhoffman-ucla/test-module/pkg/convolve"
)

func main() {
	fmt.Println("hi")

	conv := convolve.Convolve([]float64{-20,30,100,-3,5},[]float64{17,4.5,2,0.5,34}, convolve.Mode("max"))
	conv2 := convolve.Convolve([]float64{-20,30,100,-3,5},[]float64{17,4.5,2,0.5,34},  convolve.Mode("valid"))

	fmt.Println(conv)
	fmt.Println(conv2)

	fmt.Println("exe2 finished")
}